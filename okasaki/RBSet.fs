﻿module RBSet

//Maintains balance by colouring nodes red and black, and rebalancing
//after insertion. Good diagram for balancing on page 27


open TreeSet

type Colour = R | B

type Tree<'T when 'T:comparison> = 
    | E
    | T of Colour * Tree<'T> * 'T * Tree<'T>

let empty = E

let rec member' x = function
    | E -> false
    | T(_,l,y,r) ->
        if (x < y) then member' x l
        elif (x > y) then member' x r
        else true
    
//This balances a node as drawn on page 27
let balance = function
    | (B, T(R,T(R,a,x,b), y, c),z,d)
    | (B, T(R,a,x,T(R,b,y,c)),z,d) 
    | (B, a,x,T(R,T(R,b,y,c),z,d))
    | (B,a,x,T(R,b,y,T(R,c,z,d))) -> T(R,T(B,a,x,b),y,T(B,c,z,d))
    | body -> T body
        
let insert x s= 
    let rec ins = function
        | E -> T(R,E,x,E)
        | T(colour,a,y,b) as s' ->
            if (x < y) then balance (colour,ins a,y,b)
            elif (x > y) then balance (colour, a, y, ins b)
            else s'
    match ins s with
    | T(_,a,y,b) -> T(B,a,y,b)
    | E -> failwith "guarunteed to be non empty"

     
let s = empty |> insert 6 |> insert 5 |> insert 4 |> insert 3 |> insert 2 |> insert 1 |> insert 0 |> insert 1 |> insert 5

let in1 = s |> member' 1
let in7 = s |> member' 7

