﻿module Heap

/// <summary>
/// A simple leftish Heap
/// </summary>
type Heap<'T when 'T:comparison> =
    | E
    | H of int * 'T * Heap<'T> * Heap<'T>

let rank = function
    | E -> 0
    | H(r,_,_,_) -> r

let MakeT x a b=
    if (rank a > rank b) 
        then H(rank b + 1, x, a, b)
        else H(rank a + 1, x, b, a) 

let empty = E

let isEmpty = function
    | E -> true
    | _ -> false

let rec merge h1 h2 =
    match h1, h2 with
    | E, h | h,E -> h
    | H(_,x1, l,r), H(_,x2,_,_) when x1 < x2 -> MakeT x1 l (merge r h2)
    | _, H(_,x,l,r) -> MakeT x l (merge r h1)  

let insert x h = merge h (H(1,x,E,E))

exception EmptyHeap

let findMin = function
    | E -> raise EmptyHeap
    | H(_,x,_,_) -> x

let deleteMin = function
    | E -> raise EmptyHeap
    | H(_,_,l,r) -> merge l r


module test =
    let h = empty
    ignore (isEmpty h)
    let h2 = h |> insert 5 |> insert 15 |> insert 6 |> insert 2 |> insert 12
    findMin h2
    let h3 = deleteMin h2
    findMin h3
// merge insert findmin deletemin