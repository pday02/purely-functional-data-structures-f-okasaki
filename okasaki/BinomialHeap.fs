﻿module BinomialHeap

type Tree<'T when 'T : comparison> = Node of int * 'T * (Tree<'T> list)

let link t1 t2 =
    match t1, t2 with
    | Node(r,x1,c1), Node(_,x2,c2) ->
        if (x1<=x2) 
            then Node(r+1,x1,t2::c1)
            else Node(r+1,x2,t1::c2)

/// Binomial heap stores trees or different ranks represening the binary number
/// Page 21 in the book has a really good diagram of different ranks of trees.

           
type BinHeap<'T when 'T:comparison> = Tree<'T> list

exception EmptyTree

let rank = function
    | Node(r,_,_) -> r

let root = function
    | Node(_,r,_) -> r

//Used for inserting a tree that is always less than or equal to the lowest rank of the RHT
let rec insTree t = function
    | [] -> [t]
    | (t'::ts') as ts ->
        if (rank t) < (rank t') 
            then t :: ts
            else insTree (link t t') ts'
           
let insert x ts = insTree (Node(0,x,[])) ts

let rec merge ts1 ts2 =
    match ts1, ts2 with
    |  [], t | t, [] -> t
    | (t1::ts1'), (t2::ts2')->
        if (rank t1) < (rank t2)
            then t1 :: merge ts1' ts2
        elif (rank t2) < (rank t1)
            then t2 :: merge ts1 ts2'
        else
            insTree (link t1 t2) (merge ts1' ts2')

let rec removeMinTree = function
    | [t] -> t, []
    | (t::ts) -> 
        let t', ts' = removeMinTree ts
        if (root t) < (root t') then (t,ts) else (t', t::ts')
    | [] -> raise EmptyTree
        
let findMin ts = ts |> removeMinTree |> fst |> root 

let deleteMin ts = 
    match removeMinTree ts with
    | Node(_,_,ts1), ts2 ->
        merge (ts1 |> List.rev) ts2

let t0 = [] |> insert 2 |> insert 4 |> insert 2 |> insert 6 |> insert 7 |> insert 1 |> insert 13 |> insert 3 |> insert 22

let m = findMin t0

let m1 = t0 |> deleteMin |> findMin